HEPWL_BMKEXE=lhcb-gen-sim-bmk.sh
HEPWL_BMKOPTS="-c 4 -e 5" # -c replaces -n as of v0.7
HEPWL_BMKDIR=lhcb-gen-sim
HEPWL_BMKDESCRIPTION="LHCb GEN-SIM benchmark"
HEPWL_DOCKERIMAGENAME=lhcb-gen-sim-bmk
HEPWL_DOCKERIMAGETAG=v0.13 # versions >= v0.6 use common bmk driver, >= v0.9 use separate GEN/SIM scores
HEPWL_CVMFSREPOS=lhcb.cern.ch
