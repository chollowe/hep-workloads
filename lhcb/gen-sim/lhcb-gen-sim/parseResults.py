#!/usr/bin/env python

from __future__ import print_function
import os, sys
from datetime import datetime, timedelta
import numpy as np
import json
from collections import OrderedDict

# Parse SP or MP log.txt for a job to extract event timestamps
def parseLogTxt(file, debug=True, isMP=False, debug2=True):
    if debug or debug2:
        print('FILE:', file)
    current_ic = {} # [start_timestamp, #part, ##part, #vert, ##vert, 'GEN/SIM']
    summary_ic = {} # [[deltatime, #part, ##part, #vert, ##vert, 'GEN/SIM'], ...]
    magcur_ic = {} # start_timestamp
    magsum_ic = {} # deltatime
    tmd_gs_ic = {} # { 'GEN' : deltatime, 'SIM' : deltatime }
    def fixMPline(lline, isMP):
        if not isMP:
            return lline
        elif lline[0].startswith('[Worker-'):
            return lline
        else:
            found = False
            llinenew = []
            for f in lline:
                if found:
                    llinenew.append(f)
                else:
                    i = f.find('[Worker-')
                    if i != -1:
                        found = True
                        llinenew.append(f[i:])
            #print( 'WARNING! Line was:', lline )
            #print( 'WARNING! Line is:',  llinenew )
            return llinenew
    def getTime(lline, isMP):
        if not isMP:
            tm = lline[0] + ' ' + lline[1]
        else:
            tm = lline[1] + ' ' + lline[2]
        try: out = datetime.strptime(tm, '%Y-%m-%d %H:%M:%S.%f')
        except: out = datetime.strptime(tm, '%Y-%m-%d %H:%M:%S')
        ###print( 'Time', out )
        return out
    def getEvt(lline, isMP):
        if not isMP:
            out = int(lline[6].rstrip(','))
        else:
            out = int(lline[7].rstrip(','))
        ###print( 'Evt', out )
        return out
    def getCpuNum(lline, isMP):
        if not isMP:
            out=0 # there is only one process (this is not gaudiMP)
        else:
            workerstr = lline[0]
            out = int(workerstr.split('-')[-1].rstrip(']'))
        ###print( 'CpuNum', out )
        return out
    def getPartVert(lline):
        out = int(lline[len(lline)-1])
        ###print( lline[len(lline)-2], out )
        return out
    def getTimeDelta(lline, isMP):
        if not isMP:
            tmd = lline[16]
        else:
            tmd = lline[17]
        out = float(tmd)
        ###print( 'TimeDelta', out )
        return out
    s0a = 'GaussGen '
    s0b = 'MainEventGaussSim '
    s1 = 'Nr. in job'
    s2 = 'events processed'
    s3 = 'Number of extracted MCParticles'
    s4 = 'Number of extracted MCVertices'
    t0 = 'TimingAuditor'
    t1 = ' Generator '
    t2 = ' Simulation '
    m0 = ' MagneticFieldSvc '
    m1 = ' Opened magnetic field file '
    fh = open(file)
    for line in fh.readlines() :
        lline = line.split()
        # Start of GEN for new event (and possibly end of SIM for previous event)
        if line.find(s0a) != -1 and line.find(s1) != -1:
            #if debug : print( 1, line, )
            try:
                lline1 = fixMPline(lline, isMP)
                icpu = getCpuNum(lline1, isMP)
                time = getTime(lline1, isMP)
                evt = getEvt(lline1, isMP)
            except:
                raise Exception( 'ERROR parsing line %s in file %s'%(lline, file) )
            if icpu not in summary_ic :
                summary_ic[icpu] = []
            if icpu not in current_ic :
                current_ic[icpu] = [time, evt, 0, 0, 0, 0, 'GEN']
            else:
                current_ic[icpu][0] = \
                  timedelta.total_seconds(time - current_ic[icpu][0])
                summary_ic[icpu].append( current_ic[icpu] )
                current_ic[icpu] = [time, evt, 0, 0, 0, 0, 'GEN']
        # Start of MAG and SIM for first event (and end of GEN for first event)
        if line.find(m0) != -1 and line.find(m1) != -1:
            #if debug : print( 'm', line, )
            try:
                lline1 = fixMPline(lline, isMP)
                icpu = getCpuNum(lline1, isMP)
                time = getTime(lline1, isMP)
                ###evt = getEvt(lline1, isMP)
                evt = None
            except:
                raise Exception( 'ERROR parsing line %s in file %s'%(lline, file) )
            if icpu not in magcur_ic : # only process the first occurrence
                magcur_ic[icpu] = time
                if icpu not in summary_ic :
                    raise Exception( 'ERROR: icpu=%s not in summary_ic?'%icpu )
                if icpu not in current_ic :
                    raise Exception( 'ERROR: icpu=%s not in current_ic?'%icpu )
                else:
                    current_ic[icpu][0] = \
                      timedelta.total_seconds(time - current_ic[icpu][0])
                    summary_ic[icpu].append( current_ic[icpu] )
                    current_ic[icpu] = [time, evt, 0, 0, 0, 0, 'SIM']
        # Start of SIM for current event and end of GEN for current event
        # (and possibly end of MAG for first event)
        if line.find(s0b) != -1 and line.find(s1) != -1:
            #if debug : print( 1, line, )
            try:
                lline1 = fixMPline(lline, isMP)
                icpu = getCpuNum(lline1, isMP)
                time = getTime(lline1, isMP)
                evt = getEvt(lline1, isMP)
            except:
                raise Exception( 'ERROR parsing line %s in file %s'%(lline, file) )
            if icpu not in summary_ic :
                raise Exception( 'ERROR: icpu=%s not in summary_ic?'%icpu )
            if icpu not in magsum_ic : # this is the first event
                magsum_ic[icpu] = \
                  timedelta.total_seconds(time - magcur_ic[icpu])
                if icpu not in current_ic :
                    raise Exception( 'ERROR: icpu=%s not in current_ic?'%icpu )
                else:
                    current_ic[icpu][1] = evt
            else : # this is not the first event
                if icpu not in current_ic :
                    raise Exception( 'ERROR: icpu=%s not in current_ic?'%icpu )
                else:
                    current_ic[icpu][0] = \
                      timedelta.total_seconds(time - current_ic[icpu][0])
                    summary_ic[icpu].append( current_ic[icpu] )
                    current_ic[icpu] = [time, evt, 0, 0, 0, 0, 'SIM']
        # End of SIM for last event
        elif line.find(s0a) != -1 and line.find(s2) != -1:
            #if debug : print( 2, line, )
            try:
                lline1 = fixMPline(lline, isMP)
                icpu = getCpuNum(lline1, isMP)
                time = getTime(lline1, isMP)
            except:
                raise Exception( 'ERROR parsing line %s in file %s'%(lline, file) )
            current_ic[icpu][0] = \
              timedelta.total_seconds(time - current_ic[icpu][0])
            summary_ic[icpu].append( current_ic[icpu] )
        # MCParticles
        elif line.find(s3) != -1:
            #if debug : print( 3, line, )
            try:
                lline1 = fixMPline(lline, isMP)
                icpu = getCpuNum(lline1, isMP)
                part = getPartVert(lline1)
            except:
                raise Exception( 'ERROR parsing line %s in file %s'%(lline, file) )
            current_ic[icpu][2] += part
            current_ic[icpu][3] += 1
        # MCVertices
        elif line.find(s4) != -1:
            #if debug : print( 4, line, )
            try:
                lline1 = fixMPline(lline, isMP)
                icpu = getCpuNum(lline1, isMP)
                vert = getPartVert(lline1)
            except:
                raise Exception( 'ERROR parsing line %s in file %s'%(lline, file) )
            current_ic[icpu][4] += vert
            current_ic[icpu][5] += 1
        # Summary table - GEN
        elif line.find(t0) != -1 and line.find(t1) != -1:
            #if debug : print( 't1', line, )
            try:
                lline1 = fixMPline(lline, isMP)
                icpu = getCpuNum(lline1, isMP)
                tmd = getTimeDelta(lline1, isMP)
                tmd_gs_ic[icpu] = { 'GEN' : tmd }
            except:
                raise Exception( 'ERROR parsing line %s in file %s'%(lline, file) )
        # Summary table - SIM
        elif line.find(t0) != -1 and line.find(t2) != -1:
            #if debug : print( 't2', line, )
            try:
                lline1 = fixMPline(lline, isMP)
                icpu = getCpuNum(lline1, isMP)
                tmd = getTimeDelta(lline1, isMP)
                tmd_gs_ic[icpu]['SIM'] = tmd
            except:
                raise Exception( 'ERROR parsing line %s in file %s'%(lline, file) )
    if debug :
        print()
        print('iCPU=xx ', '[%10s, %6s, %6s, %2s, %6s, %2s, %3s]'%('time','evt','#part','##','#vert','##', 'G/S'), '%12s'%'time/k#part')
        print()
        for gs in 'GEN', 'SIM':
            for icpu in summary_ic:
                ###print( 'iCPU=%i'%icpu )
                for ievt in summary_ic[icpu]: # ALL
                ###for ievt in summary_ic[icpu][1:]: # SKIP FIRST
                    if ievt[6] == gs:
                        print( 'iCPU=%02i '%icpu,\
                               '[%10.3f, %6i, %6i, %2i, %6i, %2i, %3s]'%tuple(ievt), \
                               '%12.5f'%(1000*float(ievt[0])/ievt[2]) if ievt[2]>0 else '%12s'%'---' )
                print()
    summary_ic_gen = { icpu: [ievt for ievt in summary_ic[icpu] if ievt[6]=='GEN'] for icpu in summary_ic}
    summary_ic_sim = { icpu: [ievt for ievt in summary_ic[icpu] if ievt[6]=='SIM'] for icpu in summary_ic}
    #if debug or debug2:
    if debug and debug2:
        for gs, sic in ( 'GEN', summary_ic_gen ), ( 'SIM', summary_ic_sim ):
            print( '***', gs )
            summary = sum([sic[icpu] for icpu in sic], [])
            nevt = len(summary)
            summary2 = sum([sic[icpu][1:] for icpu in sic], [])
            nevt2 = len(summary2)
            print( '[TIMAUDITOR]', 'Total time', sum( tmd_gs_ic[icpu][gs] for icpu in tmd_gs_ic ) )
            print()
            for (t, s, n) in ('[ALL EVENTS]', summary, nevt), ('[SKIP FIRST]', summary2, nevt2):
                print( t, 'Total events', n )
                print( t, 'Total time[sec]', sum(ievt[0] for ievt in s) )
                print( t, 'Avg time[sec]/event:', sum(ievt[0] for ievt in s)/n )
                print( t, 'THROUGHPUT (avg events/time[msec]):', n/sum(ievt[0] for ievt in s)*1000 )
                print( t, 'Total part', sum(ievt[2] for ievt in s) )
                print( t, 'Avg part/event:', sum(ievt[2] for ievt in s)*1./n )
            print()
        magsum = sum([magsum_ic[icpu] for icpu in magsum_ic])
        print( '[FIRSTEVENT]', 'Total time in magnetic field SIM initialization', magsum )
        print()
    ###timdata_ic = summary_ic # JUST TO MAKE THIS A BIT CLEARER IN THE NEW KNL CODE...
    ###print( timdata_ic )
    ###return timdata_ic
    return summary_ic

# Extract the benchmarking scores: GEN and SIM deltas for all events except the first one
def extractBmkScores(file, debug=True, isMP=False, debug2=True):
    summary_ic = parseLogTxt(file, debug, isMP, debug2)
    summary_ic_gen = { icpu: [ievt for ievt in summary_ic[icpu] if ievt[6]=='GEN'] for icpu in summary_ic}
    summary_ic_sim = { icpu: [ievt for ievt in summary_ic[icpu] if ievt[6]=='SIM'] for icpu in summary_ic}
    # NB: ASSUME A SINGLE icpu==0 HERE FOR THE MOMENT (NO GAUSSMP)...
    totgen = sum( ievt[0] for ievt in summary_ic_gen[0][1:] )
    totsim = sum( ievt[0] for ievt in summary_ic_sim[0][1:] )
    totpar = sum( ievt[2] for ievt in summary_ic_sim[0][1:] )
    totevt = len( [ievt[0] for ievt in summary_ic_sim[0][1:]] )
    # Old version. Time per event.
    # Keep #particles as score[0] for control with 0 std, SIMtime as score[1] for main bmk score
    # [note in any case that python dictionaries may change this order by sorting on the dictionary key...]
    ###tag = ('SIM#par', 'SIMtime', 'GENtime', 'nevt')
    ###one = (totpar, totsim, totgen, totevt)
    # New version. Throughput event per wall millisecond.
    totgen0 = sum( ievt[0] for ievt in summary_ic_gen[0][0:] )
    totsim0 = sum( ievt[0] for ievt in summary_ic_sim[0][0:] )
    totevt0 = len( [ievt[0] for ievt in summary_ic_sim[0][0:]] )
    ###tag = ('GENSIM evt/wallmsec (including init)', 'GEN evt/wallmsec (excluding init)', 'SIM evt/wallmsec (excluding init)' )
    ###tag = ('GENSIM0', 'GEN', 'SIM' )
    tag = ('GENSIM0 (evts per wall msec, including 1st evt)', 'GEN (evts per wall msec, excluding 1st evt)', 'SIM (evts per wall msec, excluding 1st evt)' )
    one = (totevt0/(totgen0+totsim0)*1000, totevt/totgen*1000, totevt/totsim*1000)
    return tag, one

# Parse the full log directory of a benchmarking test
def parseBmkDir(resdir, debug=False, isMP=False, debug2=False):
    if not os.path.isdir(resdir):
        raise Exception( 'ERROR! unknown directory %s'%resdir )
    resdir=os.path.realpath(resdir)
    if debug: print( 'Iterating over', resdir )
    curlist = os.listdir(resdir)
    curlist.sort()
    all = [] # [ [gen1,sim1,par1...], [gen2,sim2,par2...], [gen3,sim3,par3...]... ]
    for d in curlist:
        if d.find('proc_') != 0 : continue
        #if d != 'proc_1' : continue # TO DEBUG!
        ijob = d[5:]
        ###print( ijob )
        logfile = resdir + '/' + d + '/' + 'out_'+ijob+'.log'
        if not os.path.isfile:
            if debug: print( 'WARNING: ', logfile, 'not found' )
            continue
        if debug: print( 'Parsing', logfile )
        tag, one = extractBmkScores(logfile, debug, isMP, debug2)
        # NB: SCORES FOR THE PRESENT LHCB TEST WITH 5 EVENTS!
        #exptotpar = 39588
        #totgen, totsim, totpar = bmkScores
        #if totpar != exptotpar:
        #    print( 'FATAL ERROR! Wrong #particles', totpar, 'should be', exptotpar )
        #    print( 'FATAL ERROR! The benchmarking test failed!' )
        #    raise Exception( 'FATAL ERROR' )
        #print( totgen, totsim )
        all.append(one)
    if all == [] :
        raise Exception( 'ERROR! no logs found in directory %s'%resdir )
    all = np.transpose(all) # [ [gen1,gen2,gen3...], [sim1,sim2,sim3...], [par1,par2,par3...]... ]
    ###print( all )
    # Build the json summary manually to preserve the order of fields
    jsum = ''
    for t,s in zip(tag,all) :
        if jsum == '' : jsum += '{ '
        else : jsum += ' , '
        jsum += '"' + t + '" : '
        jsum += '{ '
        jsum += '"score" : ' + str(round(np.sum(s),4)) + ' , '
        ###jsum += '"len" : ' + str(np.size(s)) + ' , '
        jsum += '"avg" : ' + str(round(np.mean(s),4)) + ' , '
        jsum += '"median" : ' + str(round(np.median(s),4)) + ' , '
        jsum += '"min" : ' + str(round(np.min(s),4)) + ' , '
        jsum += '"max" : ' + str(round(np.max(s),4))
        ###jsum += ' , ' + '"std" : ' + str(round(np.min(s),4))
        jsum += ' }'
    jsum += ' }'
    print( jsum )
    all = json.loads(jsum)
    ###print( all )
    return all

#-----------------------------------------------------------------------------------------

if __name__ == '__main__':

    # STANDALONE TESTS
    parseLogTxt('jobs/good_1/proc_1/out_1.log', debug=True, isMP=False)
    parseBmkDir('jobs/good_1', debug=False, debug2=False)
    ###parseLogTxt('jobs/good_2/proc_1/out_1.log', debug=True, isMP=False)
    ###parseBmkDir('jobs/good_2', debug=False, debug2=False)

    # PRODUCTION (achieved by python -c '...')
    ###parseBmkDir('.', debug=True, debug2=True)
