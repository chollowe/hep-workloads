HEPWL_BMKEXE=alice-gen-sim-bmk.sh
HEPWL_BMKOPTS="-c 4 -e 1" # -c replaces -n as of v0.10
HEPWL_BMKDIR=alice-gen-sim
HEPWL_BMKDESCRIPTION="ALICE pp GEN-SIM"
HEPWL_DOCKERIMAGENAME=alice-gen-sim-bmk
HEPWL_DOCKERIMAGETAG=v0.16 # versions >= v0.11 use common bmk driver
HEPWL_CVMFSREPOS=alice.cern.ch
