#!/bin/bash

###set -x # verbose

set -e # immediate exit on error

#------------------------------------------------------------------------------

# Get a CERN SSO cookie (from project HEP_OSlibs)
function get_gitlab_cookie()
{
  # Check arguments: expect "${ckf_var}"
  if [ "$1" == "" ] || [ "$2" != "" ]; then
    echo "PANIC! Wrong arguments to gitlab_token: \"$*\"" > /dev/stderr
    exit 1
  fi
  local _fva=$1

  # Get a CERN SSO cookie for the current Kerberos user against gitlab.cern.ch 
  if [ -d ~/private/ ]; then
    local _CKF=~/private/TMP-cookie.txt
    \rm -rf $_CKF
  else
    echo "WARNING! Directory ~/private/ does not exist: use /tmp" > /dev/stderr
    local _CKF=$(mktemp)
  fi
  cern-get-sso-cookie --krb -u https://gitlab.cern.ch/profile/account -o $_CKF
  echo "Retrieved cookie into $_CKF"
  eval ${_fva}=${_CKF}
}

#------------------------------------------------------------------------------

function usage(){
  echo "Usage: $0 [-n <jobnam (default:good_1)>] [-d] <gitlabJobUrl>"
  echo "Example: $0 https://gitlab.cern.ch/valassi/hep-workloads/-/jobs/4783122 -d"
  echo ""
  echo "  -n <jobnam>        job directory name (default: good_1)"
  echo "  -d                 delete <bmkdir>/jobs/<jobnam>/* if directory exists"
  echo "  <gitlabJobUrl>     job url https://gitlab.cern.ch/<owner>/hep-workloads/-/jobs/<jobid>"
  echo ""
  exit 1
}

# Process input arguments
#--------------------------

deljob=
joburl=
jobnam=
while [ "$1" != "" ]; do
  if [ "$1" == "-n" ] && [ "$2" != "" ] && [ "$jobnam" == "" ]; then
    jobnam=$2
    shift; shift
  elif [ "$1" == "-d" ] && [ "$deljob" == "" ]; then
    deljob=1
    shift
  elif [ "${1##https://gitlab.cern.ch/}" != "$1" ] && [ "$joburl" == "" ]; then
    joburl=$1
    shift
  else  
    usage
  fi
done
if [ "$jobnam" == "" ]; then jobnam="good_1"; fi
if [ "$joburl" == "" ]; then usage; fi

echo "[$(basename $0)] deljob: ${deljob}"
echo "[$(basename $0)] joburl: ${joburl}"
echo "[$(basename $0)] jobnam: ${jobnam}"

# Get gitlab cookie
#--------------------------

get_gitlab_cookie CKF
if [ ! -f $CKF ]; then
  echo "ERROR! File $CKF not found"
  exit 1
fi

# Create tmp job dir
#--------------------------

scrdir=$(cd $(dirname $0); pwd)
tmpdir=$(mktemp -d)/jobs/${jobnam}
mkdir -p $tmpdir
cd $tmpdir
echo "[$(basename $0)] tmpdir: ${tmpdir}"

# Fetch gitlab artifacts
#--------------------------

echo ${joburl} > joburl.txt

# Keep both json (cvmfs-shrink and standalone), including sub-scores, and keep all Parsing messages
curl -s -L --cookie $CKF --cookie-jar $CKF ${joburl}/raw | egrep '(Parsing results|"copies"|"score")' > cishortlog.txt
# Keep only the first (cvmfs-shrink) json, excluding sub-scores, and do not keep any Parsing messages
###curl -s -L --cookie $CKF --cookie-jar $CKF ${joburl}/raw | egrep '"copies"' | head -1 > cishortlog.json

curl -s -L --cookie $CKF --cookie-jar $CKF ${joburl}/artifacts/download -odownload.zip

# Delete gitlab cookie
#--------------------------

\rm -f $CKF

# Process gitlab artifacts
#--------------------------

unzip download.zip
\rm -f download.zip

tar -xzf results.tar.gz
\rm -f results.tar.gz

\mv scratch/logs/CI-JOB-*/results .
\rm -rf scratch

# Keep only the first set of test results (cvmfs-shrink image)
# Do not keep the second set of test results (standalone docker image)
# Do not keep the third set of test results (standalone singularity image)
if [ "$(find results -mindepth 1 -maxdepth 1 -type d | wc -l)" != "2" ] && [ "$(find results -mindepth 1 -maxdepth 1 -type d | wc -l)" != "3" ]; then
  echo -e "\nERROR! Directory ${pwd}/results should contain two or three directories\n"
  exit 1
fi  
test1="$(find results -mindepth 1 -maxdepth 1 -type d | sort | head -1)"
\mv $test1/* .
if ls results/cvmfs*.spec.txt &> /dev/null; then
  \mv results/cvmfs*.spec.txt .
fi
\rm -rf results

# Identify the bmk
#--------------------------

bmkspecs=$(cd $scrdir/..; ls */*/*.spec)
bmknam=
for bmkspec in $bmkspecs; do
  if [ -f $(basename $bmkspec .spec)_summary.json ]; then
    bmknam=$(basename $bmkspec .spec)
    bmkdir=$(dirname $bmkspec)/$bmknam
    break
  fi
done

if [ "${bmknam}" == "" ]; then
  echo -e "\nERROR! Could not identify benchmark from json files" *_summary.json
  exit 1
fi

jsonsummary=${bmknam}_summary.json
ln -sf ${jsonsummary} summary.json

# Move to job dir
#--------------------------

jobdir=${scrdir}/../${bmkdir}/jobs/${jobnam}
echo "[$(basename $0)] jobdir: ${jobdir}"
if [ -d ${jobdir} ]; then
  if [ "${deljob}" == "1" ]; then
    echo -e "\nWARNING! Directory ${jobdir} exists: delete all its contents"
    \rm -rf ${jobdir}/*
  else
    echo -e "\nERROR! Directory ${jobdir} exists: use option -d to delete all its contents"
    exit 1
  fi
else
  mkdir -p ${jobdir}
fi
cd ${jobdir}
mv ${tmpdir}/* .

# Dump results
#--------------------------

echo -e "\n[$(basename $0)] Retrieved new reference results from ${joburl} into $(pwd)\n"
ls -lR

echo -e "\n[$(basename $0)] cishortlog.txt:\n"
cat cishortlog.txt

echo -e "\n[$(basename $0)] ${jsonsummary}:\n"
cat ${jsonsummary}
echo ""
