#!/bin/bash

###set -x # verbose

set -e # immediate exit on error

#------------------------------------------------------------------------------

# Get a CERN SSO cookie (from project HEP_OSlibs)
function get_gitlab_cookie()
{
  # Check arguments: expect "${ckf_var}"
  if [ "$1" == "" ] || [ "$2" != "" ]; then
    echo "PANIC! Wrong arguments to gitlab_token: \"$*\"" > /dev/stderr
    exit 1
  fi
  local _fva=$1

  # Get a CERN SSO cookie for the current Kerberos user against gitlab.cern.ch 
  if [ -d ~/private/ ]; then
    local _CKF=~/private/TMP-cookie.txt
    \rm -rf $_CKF
  else
    echo "WARNING! Directory ~/private/ does not exist: use /tmp" > /dev/stderr
    local _CKF=$(mktemp)
  fi
  cern-get-sso-cookie --krb -u https://gitlab.cern.ch/profile/account -o $_CKF
  echo "Retrieved cookie into $_CKF"
  eval ${_fva}=${_CKF}
}

#------------------------------------------------------------------------------

function usage(){
  echo "Usage: $0 [-n <jobnam (default:good_1)>] <gitlabPipelineUrl>"
  echo "Example: $0 https://gitlab.cern.ch/hep-benchmarks/hep-workloads/pipelines/1018245"
  echo ""
  echo "  -n <jobnam>          job directory name (default: good_1)"
  echo "  <gitlabPilelineUrl>  pipeline url https://gitlab.cern.ch/<owner>/hep-workloads/pipelines/<pipelineid>"
  echo ""
  exit 1
}

# Process input arguments
#--------------------------

pipurl=
jobnam=
while [ "$1" != "" ]; do
  if [ "$1" == "-n" ] && [ "$2" != "" ] && [ "$jobnam" == "" ]; then
    jobnam=$2
    shift; shift
  elif [ "${1##https://gitlab.cern.ch/}" != "$1" ] && [ "${1%%/hep-workloads/pipelines/*}" != "$1" ] && [ "$pipurl" == "" ]; then
    pipurl=$1
    shift
  else  
    usage
  fi
done
if [ "$jobnam" == "" ]; then jobnam="good_1"; fi
if [ "$pipurl" == "" ]; then usage; fi

echo "[$(basename $0)] pipurl: ${pipurl}"
echo "[$(basename $0)] jobnam: ${jobnam}"

# Get gitlab cookie
#--------------------------

get_gitlab_cookie CKF
if [ ! -f $CKF ]; then
  echo "ERROR! File $CKF not found"
  exit 1
fi

# Fetch pipeline
#--------------------------

# Get successful jobs from raw html report (cannot access the gitlab API with CKF)
jobs=$(curl -s -L --cookie $CKF --cookie-jar $CKF ${pipurl} | grep Success | grep /hep-workloads/-/jobs | cut -d' ' -f7 | cut -d\" -f 2 )

# Process jobs individually
scrdir=$(cd $(dirname $0); pwd)
for job in $jobs; do
  joburl=https://gitlab.cern.ch${job}
  echo -e "\n=============================================================\n${joburl}"
  echo -e "=============================================================\n"
  $scrdir/referenceFromJobUrl.sh -n $jobnam $joburl -d   
done
