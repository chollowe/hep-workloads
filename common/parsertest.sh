#!/bin/bash

PARSERTEST_DIR=$(dirname $(readlink -f $0) )
# Input arguments
function usage(){
  echo "Usage: $0 [<workload-directory> (default: .)]"
  exit 1
}
if [ "$1" != "" ]; then
  if [ -d $1 ]; then
    cd $1
    shift
  else
    echo "ERROR! directory $1 not found"
    usage
  fi
fi
if [ "$1" != "" ]; then usage; fi

# Launch this script from the directory containing parseResults.sh and jobs
echo "Workload directory: $(pwd)"
if [ -f parseResults.sh ]; then
  source parseResults.sh # load the parseResults function
else
  echo "ERROR! file $(pwd)/parseResults.sh not found"
  exit 1
fi
if [ ! -d jobs ]; then
  echo "ERROR! directory $(pwd)/jobs not found"
  exit 1
fi
jobsdir=$(pwd)/jobs
echo -e "\nJobs directory: $jobsdir"
ls -l $jobsdir

# The environment variable APP is used by parseResults to name the json file ${APP}_summary.json
# In production (CI, benchmarks, refjobs) APP is equal to <vo>-<workload>
# In this test, set APP to a different name TEST-<vo>-<workload>
APPprod=$(basename $(pwd))
APPtest=TEST-${APPprod}
APP=${APPtest} # this is used by function parseResults

#
# LOOP OVER JOBS
#
status=0
jobs=$(cd $jobsdir; ls | grep -v refjob)
for job in $jobs; do

  # Examine only the good_1 job (for the moment)
  refdir=$jobsdir/$job
  if [ ! -d ${refdir} ]; then
    echo "ERROR! directory ${refdir} not found"
    exit 1
  fi
  echo -e "\n======================================================="
  echo -e "\nReference job directory: ${refdir}"

  # Define NCOPIES, NTHREADS, NEVENTS_JSON from inputs.log
  if [ -f ${refdir}/inputs.log ]; then
    source ${refdir}/inputs.log
  else
    echo "ERROR! file ${refdir}/inputs.log not found"
    exit 1
  fi

  # Define BMKDIR as the directory containing version.json
  BMKDIR=$refdir

  # Export variables (e.g. for the parseResults.py subprocess)
  DEBUG=1
  for var in NCOPIES NTHREADS NEVENTS_THREAD BMKDIR DEBUG APP; do
    export $var
  done

  # Parse results and produce the new json
  cd $refdir
  parseResults 0 > /dev/null
  status1=$?
  ls -l *${APPprod}_summary*

  # Dump the new json and the differences to the old json
  status2=
  for suffix in _summary.json _summary_new.json _summary_old.json; do
    prodjson=${APPprod}${suffix}
    testjson=${APPtest}${suffix}
    if [ -f ${testjson} ]; then
      echo -e "\n${testjson}:"
      python -c "import yaml; print yaml.dump( yaml.safe_load(open('${testjson}')))"
      if [ -f ${prodjson} ]; then
	if [ "$status2" == "" ]; then status2=0; fi
	echo -e "\nDIFFERENCES to ${prodjson}:"
	$PARSERTEST_DIR/json-differ.py ${prodjson} ${testjson}
	#diff ${testjson} ${prodjson}
	if [ $? -eq 0 ]; then
	  echo "[None]" 
	else 
	  status=$((status+1))
	  status2=$((status2+1))
	fi
      else
	echo -e "\nWARNING! reference ${prodjson} not found"
      fi
    fi
  done

  # Clean up and dump test results
  #\rm -f *TEST*json
  echo -e "\nParser test completed for '$job' (parse exit code=$status1; test exit code=$status2)"

done
echo -e "\n======================================================="
echo -e "\nParser test completed (test exit code=$status)"
echo -e "\n======================================================="
exit $status
