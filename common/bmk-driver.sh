if [ "$BASH_SOURCE" = "$0" ]; then echo "ERROR! This script ($0) was not sourced"; exit 1; fi
if [ "$BASH_SOURCE" = "" ]; then echo "ERROR! This script was not sourced from bash"; return 1; fi

bmkDriver=$(basename ${BASH_SOURCE})
bmkScript=$(basename $0)
BMKDIR=$(cd $(dirname $0); pwd)

function advertise_bmkdriver(){
  echo -e "\n========================================================================"
  echo -e "[$bmkDriver] $(date) entering common benchmark driver"
  echo -e "========================================================================\n"
  echo -e "[$bmkDriver] entering from $bmkScript\n"
  # Dump workload-specific directory
  echo -e "[$bmkDriver] benchmark directory BMKDIR=${BMKDIR}:\n"
  ls -lRt $BMKDIR
  if [ -d $BMKDIR/../data ]; then
    echo -e "\n[$bmkDriver] data directory ${BMKDIR}/../data:\n"
    ls -lRt $BMKDIR/../data
  fi
  echo
}

# Check that mandatory functions exist or load them otherwise
function check_mandatory_functions(){
  # Check that function doOne has been defined
  if [ "$(type -t doOne)" != "function" ]; then
    echo "[$bmkDriver] ERROR! Function 'doOne' must be defined in $bmkScript" # internal error (missing code)
    exit 1;
  fi
  # Check that function parseResults has been defined, otherwise load it from parseResults.sh
  if [ "$(type -t parseResults)" != "function" ]; then
    echo "[$bmkDriver] load parseResults.sh (function 'parseResults' is not defined in $bmkScript)"
    if [ -f ${BMKDIR}/parseResults.sh ]; then
      echo -e "[$bmkDriver] sourcing ${BMKDIR}/parseResults.sh\n"
      . ${BMKDIR}/parseResults.sh
      if [ "$(type -t parseResults)" != "function" ]; then
        echo "[$bmkDriver] ERROR! Function 'parseResults' must be defined in $bmkScript or parseResults.sh" # internal error (missing code)
        exit 1;
      fi
    else
      echo -e "[$bmkDriver] ERROR! 'parseResults' not defined and ${BMKDIR}/parseResults.sh not found\n" # internal error (missing code)
      exit 1
    fi
  fi
}

# Check that mandatory variables have been defined (default values)
function check_mandatory_variables(){
  # Variables NCOPIES, NTHREADS, NEVENTS_THREAD have default values specific to each benchmark
  for var in NCOPIES NTHREADS NEVENTS_THREAD; do
    if [ "${!var}" == "" ]; then
      echo "[$bmkDriver] ERROR! A default value of $var must be set in $bmkScript" # internal error (missing code)
      exit 1;
    fi
  done
  echo
}

# Variables USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREAD are empty by default
USER_NCOPIES=
USER_NTHREADS=
USER_NEVENTS_THREADS=

# Variable resultsDir has default value /results
# Variables skipSubDir and DEBUG are 0 by default
resultsDir=/results
skipSubDir=0
DEBUG=0

function advertise_user_defined_variables(){
  for var in NCOPIES NTHREADS NEVENTS_THREAD; do
    echo "Default (from $bmkScript): $var=${!var}"
  done
  echo
  for var in USER_NCOPIES USER_NTHREADS USER_NEVENTS_THREAD; do
    echo "Default (from $bmkDriver): $var=${!var}"
  done
  echo
  for var in resultsDir skipSubDir DEBUG; do
    echo "Default (from $bmkDriver): $var=${!var}"
  done
}

# Usage function
function usage(){
  echo ""
  echo "Usage: $0 [-w <resultsDir>] [-W] [-c <NCOPIES>] [-t <NTHREADS>] [-e <NEVENTS_PER_THREAD>] [-d] [-h]"
  echo "  -w <resultsDir>     : results directory (default: /results , current: $resultsDir)"
  echo "  -W                  : store results in <resultsDir> directly (default: 0 , current: $skipSubDir)"
  echo "  -c <NCOPIES>        : # identical copies (default $NCOPIES)"
  echo "  -t <NTHREADS>       : # threads (or processes, or threads*processes) per copy (default $NTHREADS)"
  echo "  -e <NEVENTS_THREAD> : # events per thread (default $NEVENTS_THREAD)"
  echo "  -d                  : debug mode (current: $DEBUG)"
  echo "  -h                  : display this help and exit"
  echo ""
  if [ $NTHREADS -eq 1 ]; then
    echo "NTHREADS : the default value NTHREADS=1 of this parameter cannot be changed"
    echo "           (single-threaded single-process workload application)"
    echo ""
  fi
  echo "Without -W (default): results are stored in a new subdirectory of <resultsDir>:"
  echo "  <resultsDir>/<uniqueid>/*.json"
  echo "  <resultsDir>/<uniqueid>/proc_1/*.log"
  echo "  <resultsDir>/<uniqueid>/proc_.../*.log"
  echo "  <resultsDir>/<uniqueid>/proc_<COPIES>/*.log"
  echo "With -W (e.g. in the CI): results are stored in <resultsDir> directly:"
  echo "  <resultsDir>/*.json"
  echo "  <resultsDir>/proc_1/*.log"
  echo "  <resultsDir>/proc_.../*.log"
  echo "  <resultsDir>/proc_<NCOPIES>/*.log"
  echo ""
  echo "Without -w (default) and without -W: <resultsDir> is /results"
  echo "Without -w (default) and with -W: <resultsDir> is a tmp directory /tmp/xxxx"
  echo ""
  if [ "$(type -t usage_detailed)" == "function" ]; then
    echo -e "\nDetailed Usage:\n----------------\n"
    ( usage_detailed ) # as a subprocess, just in case this has a 0 exit code...
  fi
  echo -e "DESCRIPTION\n"
  if [ -e $BMKDIR/DESCRIPTION ]; then
      cat $BMKDIR/DESCRIPTION
  else
      echo "Sorry there is not description included."
  fi
  echo ""
  exit 1 # early termination (help or invalid arguments to benchmark script)
}

#####################
### HERE MAIN STARTS
#####################

# Parse the input arguments
callUsage==
while getopts "c:t:e:w:Wdh" o; do
  case ${o} in
    c)
      if [ $OPTARG -gt 0 ]; then
        USER_NCOPIES=$OPTARG
      else
        echo "[$bmkDriver] ERROR! Invalid argument '-c $OPTARG' (must be > 0)"
        exit 1 # early termination (invalid arguments to benchmark script)
      fi
      ;;
    t)
      if [ $OPTARG -gt 0 ]; then
        USER_NTHREADS=$OPTARG
        if [ $NTHREADS -eq 1 ] && [ $USER_NTHREADS -ne 1 ]; then
          echo "[$bmkDriver] ERROR! Invalid argument '-t $OPTARG' (default NTHREADS=1 cannot be changed)"
          exit 1 # early termination (invalid arguments to benchmark script)
        fi
      else
        echo "[$bmkDriver] ERROR! Invalid argument '-t $OPTARG' (must be > 0)"
        exit 1 # early termination (invalid arguments to benchmark script)
      fi
      ;;
    e)
      if [ $OPTARG -gt 0 ]; then
        USER_NEVENTS_THREAD=$OPTARG
      else
        echo "[$bmkDriver] ERROR! Invalid argument '-e $OPTARG' (must be > 0)"
        exit 1
      fi
      ;;
    w)
      resultsDir=$OPTARG
      ;;
    W)
      skipSubDir=1
      ;;
    d)
      DEBUG=1
      ;;
    *)
      callUsage=1 # need to do in this way to enable parsing of all arguments (???)
      ;;
  esac
done

if [ "$DEBUG" == 1 ]; then
  echo -e "\n[$bmkDriver] Parse input arguments '$@'\n"
  advertise_bmkdriver
  advertise_user_defined_variables
fi

# No other input arguments are expected
shift $((OPTIND -1))
if [ "$1" != "" ]; then usage; fi

if [ "$callUsage" == "1" ]; then usage; fi

# Check that mandatory functions exist or load them otherwise
check_mandatory_functions

# Check that mandatory variables have been defined (default values)
check_mandatory_variables

# Dump all relevant variables after parsing the input arguments
for var in USER_NCOPIES USER_NTHREADS USER_NEVENTS_THREAD; do
  echo "Current value: $var=${!var}"
done
echo
for var in resultsDir skipSubDir DEBUG; do
  echo "Current value: $var=${!var}"
done
echo

# Variable resultsDir must be set through command line options
# Backward compatibility: all benchmarks initially hardcoded 'RESULTS_DIR=/results'
if [ "${resultsDir}" == "" ]; then
  ###echo "[$bmkDriver] ERROR! resultsDir not specified ('-w' missing)"
  ###exit 1 # early termination (invalid arguments to benchmark script)
  if [ "$skipSubDir" == "1" ]; then
    echo -e "[$bmkDriver] WARNING! resultsDir not specified ('-w' missing), but '-W' is present: create a directory in /tmp\n"
    resultsDir=$(mktemp -d)
  else
    echo -e "[$bmkDriver] WARNING! resultsDir not specified ('-w' missing) and '-W' is missing: assume '/results'\n"
    resultsDir=/results
  fi
fi

# Check that resultsDir is an existing directory
if [ ! -d ${resultsDir} ]; then
  mkdir -p ${resultsDir}
  if [ "$?" != "0" ]; then
    echo "[$bmkDriver] ERROR! directory '${resultsDir}' not found and could not be created"
    exit 1 # early termination (cannot start processing)
  fi
fi

# Status code of the validateInputArguments and doOne steps
# fail<0 : validateInputArguments failed
# fail>0 : doOne failed
# fail=0 : OK
fail=0

# Call function validateInputArguments if it exists
if [ "$(type -t validateInputArguments)" != "function" ]; then
  echo -e "[$bmkDriver] function 'validateInputArguments' not found: use input arguments as given\n"
  if [ "$USER_NCOPIES" != "" ]; then NCOPIES=$USER_NCOPIES; fi
  if [ "$USER_NTHREADS" != "" ]; then NTHREADS=$USER_NTHREADS; fi # already checked that USER_NTHREADS must be 1 if NTHREADS is 1
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
else
  echo -e "[$bmkDriver] function 'validateInputArguments' starting\n"
  if ! validateInputArguments; then fail=-1; fi
  echo -e "\n[$bmkDriver] function 'validateInputArguments' completed (status=$fail)\n"
fi

# Set baseWDir and create it if necessary
if [ "$skipSubDir" == "1" ]; then
  baseWDir=${resultsDir}
  echo -e "[$bmkDriver] base working directory : $baseWDir\n"
else
  baseWDir=${resultsDir}/$(basename $0 -bmk.sh)-c${NCOPIES}-e${NEVENTS_THREAD}-$(date +%s)_$(((RANDOM%9000)+1000))
  echo -e "[$bmkDriver] base working directory : $baseWDir\n"
  if ! mkdir $baseWDir; then
    echo "[$bmkDriver] ERROR! directory '${baseWDir}' cannot be created"
    exit 1 # early termination (cannot start processing)
  fi
fi
baseWDir=$(cd $baseWDir; pwd)

# Dump all relevant variables after validating the input arguments
# Keep a copy on a separate log too for parser tests on previous logs
touch $baseWDir/inputs.log
for var in NCOPIES NTHREADS NEVENTS_THREAD; do
  if [ "${!var}" == "" ] || ! [[ ${!var} =~ ^[0-9]+$ ]] || [ ! ${!var} -gt 0 ]; then
    echo "[$bmkDriver] ERROR! Invalid value $var=${!var}"
    exit 1;
  fi
  echo "Current value: $var=${!var}"
  echo "$var=${!var}" >> $baseWDir/inputs.log
done
echo

# Keep a copy of the version.json file for parser tests on previous logs
if [ -f $BMKDIR/version.json ]; then
  cp $BMKDIR/version.json $baseWDir
fi

# Define APP before doOne (BMK-152) and parseResults
APP=$(basename ${BMKDIR}) # or equivalently here $(basename $0 -bmk.sh)
echo -e "[$bmkDriver] APP=${APP}\n"

# Wrapper for the doOne function
function doOneWrapper(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then
    echo -e "[$bmkDriver] ERROR! Invalid arguments '$@' to doOneWrapper" # internal error (inconsistent code)
    return 1 # NB: return or exit are equivalent here because doOneWrapper is executed as a subprocess
  fi
  echo -e "\n[doOneWrapper ($1)] $(date) : process $1 started"
  ###sleep 5 # this is not needed if the list of jobs is compiled from all '$!'
  workDir=$(pwd)/proc_$1 # current directory is $baseWDir here
  echo -e "[doOneWrapper ($1)] workdir is ${workDir}"
  if ! mkdir -p $workDir || ! cd $workDir; then
    echo -e "\n[doOneWrapper ($1)] $(date) : process $1 failed (cannot create workdir)\n"
    return 1
  fi
  log=${workDir}/doOneWrapper_$1.log
  echo -e "[doOneWrapper ($1)] logfile is $log"
  if ! touch $log ; then
    echo -e "\n[doOneWrapper ($1)] $(date) : process $1 failed (cannot create logfile)\n"
    return 1
  fi
  echo -e "[doOneWrapper ($1)] $(date) : process $1 configured" 2>&1 | tee -a $log # configured means that log exists
  mkdir $workDir/HOME
  export HOME=$workDir/HOME # avoid writing to /root in read-only docker or to host HOME in singularity (BMK-166)
  echo -e "[doOneWrapper ($1)] HOME=$HOME" 2>&1 | tee -a $log
  cd -P /proc/self && basename $PWD | ( read thispid; \
    echo -e "[doOneWrapper ($1)] current process pid is $thispid" 2>&1 | tee -a $log ) # see https://stackoverflow.com/a/15170225
  cd - > /dev/null
  local pid=$(cat $log | grep "current process pid is" | sed -e "s/.*current process pid is //")
  local parsertest=0 # hardcoded: 0 => doOne (default); 1 => test the parser on old logs and bypass doOne (BMK-152)
  if [ $parsertest -eq 0 ]; then
    if [ "$(whoami)" == "root" ] && cat /proc/self/cgroup | cut -d/ -f2 | grep docker > /dev/null; then
      echo -e "[doOneWrapper ($1)] inside docker - run doOne as bmkuser\n" 2>&1 | tee -a $log
      export -f doOne
      chown -R bmkuser:bmkuser $workDir 2>&1 | tee -a $log
      su bmkuser -s /bin/bash -c "doOne $1" 2>&1 | tee -a $log
      local status=${PIPESTATUS[0]} # NB do not use $? if you pipe to tee!
      chown -R root:root $workDir 2>&1 | tee -a $log
    else
      echo -e "[doOneWrapper ($1)] not inside docker - run doOne as $(whoami)\n" 2>&1 | tee -a $log
      doOne $1 2>&1 | tee -a $log
      local status=${PIPESTATUS[0]} # NB do not use $? if you pipe to tee!
    fi
  else
    cp -dpr $BMKDIR/jobs/refjob/proc_$1/* .
    local status=$?
    \rm -f *${APP}*.json
    echo -e "[doOneWrapper ($1)] DUMMY doOne: copy old logs for parser tests (BMK-152)"
  fi
  if [ "$status" == "0" ]; then
    echo -e "\n[doOneWrapper ($1)] $(date) : process $1 (pid=$pid) completed ok\n" 2>&1 | tee -a $log
    return 0
  else
    echo -e "\n[doOneWrapper ($1)] $(date) : process $1 (pid=$pid) failed\n" 2>&1 | tee -a $log
    return 1
  fi
}

# Export variables to the doOne subprocesses
for var in NCOPIES NTHREADS NEVENTS_THREAD BMKDIR DEBUG APP; do
  export $var
done

# Spawn doOne subprocesses (unless validateInputArguments failed)
if [ $fail -eq 0 ]; then

  # Spawn subprocesses (and keep track of their list of them using '$!')
  echo -e "------------------------------------------------------------------------"
  echo -e "[$bmkDriver] spawn $NCOPIES processes"
  echo -e "------------------------------------------------------------------------\n"
  jobs=""
  for i in $(seq 1 $NCOPIES); do
    ( cd $baseWDir; doOneWrapper $i ) &
    ipid=$!
    [ $DEBUG -gt 0 ] && echo -e "[$bmkDriver] spawned process $i with pid $ipid"
    jobs="$jobs $ipid"
    sleep 0.1 # stagger job creation by 100ms
  done

  # Wait for all subprocesses to complete and check their exit codes
  # [NB: do not use 'jobs -p': some jobs may be missing if already completed]
  [ $DEBUG -gt 0 ] && echo -e "\n[$bmkDriver] $(date) ... waiting for spawned processes with pid's$jobs\n"
  wait $jobs > /dev/null 2>&1
  fail=0 # unnecessary but harmless (this code is only executed if $fail -eq 0)
  for i in $(seq 1 $NCOPIES); do
    if [ $(cat $baseWDir/proc_$i/doOneWrapper_$i.log | grep "[doOneWrapper ($i)]" | grep "completed ok" | wc -l) -ne 1 ]; then
      let "fail+=1"
    fi
  done
  echo -e "\n------------------------------------------------------------------------"
  if [ $fail -gt 0 ]; then
    echo "[$bmkDriver] ERROR! $fail processes failed (out of $NCOPIES)"
  else
    echo "[$bmkDriver] all $NCOPIES processes completed successfully"
  fi
  echo -e "------------------------------------------------------------------------\n"

# Skip the doOne step if validateInputArguments failed
else
  echo -e "[$bmkDriver] validateInputArguments failed: skip doOne processing"
fi

# Parse results and generate summary using function parseResults
# - parseResults is started in the base working directoy
# - the number of failed jobs is passed to parseResults as input parameter
# - if a separate function generateSummary exists, it must be internally called by parseResults
# - the environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
cd $baseWDir
echo -e "[$bmkDriver] parse results and generate summary: starting"
echo -e "[$bmkDriver] current directory : $(pwd)\n"
parseResults $fail
parse=$?
echo -e "\n[$bmkDriver] parse results and generate summary: completed (status=$parse)"

# Validate json files syntax (BMK-137)
cd $baseWDir
echo -e "\n[$bmkDriver] json file validation: starting"
json=0
jsonFile=$baseWDir/${APP}_summary.json
jsonFile_new=$baseWDir/${APP}_summary_new.json
if [ ! -f ${jsonFile} ]; then
  echo -e "[$bmkDriver] ERROR! json file '${jsonFile}' not found"
  json=1
else
  echo "[$bmkDriver] lint json file '${jsonFile}' syntax using jq"
  if ! jq '.' -c < ${jsonFile}; then
    echo "[$bmkDriver] json file '${jsonFile}' lint validation failed"
    json=1
  fi
fi
if [ -f ${jsonFile_new} ]; then
  echo "[$bmkDriver] lint json file '${jsonFile_new}' syntax using jq"
  if ! jq '.' -c < ${jsonFile_new}; then
    echo "[$bmkDriver] json file '${jsonFile_new}' lint validation failed"
    json=1
  fi
fi
echo -e "[$bmkDriver] json file validation: completed (status=$json)\n"

# NB: This script is meant to be sourced, it does not return or exit at the end
if [ $parse -ne 0 ] || [ $fail -ne 0 ] || [ $json -ne 0 ]; then
  bmkStatus=1
else
  bmkStatus=0
fi
echo -e "[$bmkDriver] exiting back to $bmkScript"
echo -e "\n========================================================================"
echo -e "[$bmkDriver] $(date) exiting common benchmark driver (status=$bmkStatus)"
echo -e "========================================================================\n"
exit $bmkStatus
