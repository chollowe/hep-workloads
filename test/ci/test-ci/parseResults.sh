# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# [NB: if a separate function generateSummary exists, it must be internally called by parseResults]
# Input argument $1: status code <fail> from validateInputArguments and doOne steps:
# - <fail> < 0: validateInputArguments failed
# - <fail> > 0: doOne failed (<fail> processes failed out of $NCOPIES)
# - <fail> = 0: OK
# Return value: please return 0 if parsing was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# The environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[parseResults] ERROR! Invalid arguments '$@' to parseResults"; return 1; fi
  local failedProc=$1
  echo "[parseResults] parse results and generate summary (previous status: $1)"
  echo "[parseResults] current directory: $(pwd)"
  #-----------------------
  # Parse results
  #-----------------------
  echo "[parseResults] available log files:" proc_*/out_*.log
  local failedParse=0
  for f in proc_*/out_*.log; do
    echo -e "\n[parseResults] --> Parsing results from $f"
    if ! cat $f; then
      echo "[parseResults] ERROR parsing file $f"
      let "failedParse+=1"
    fi
  done
  local score=1 # hardcoded (dummy)
  local msg="OK"
  if [ $failedProc -gt 0 ]; then
    msg="ERROR: $failedProc processes failed"
  elif [ $failedParse -gt 0 ]; then
    msg="ERROR: $failedParse parse errors"
  fi
  #-----------------------
  # Generate json summary
  #-----------------------
  # Generate the summary json
  echo -e "\n[parseResults] generate json summary"  
  local app="\"UNKNOWN\""
  if [ -f $BMKDIR/version.json ]; then app=$(cat $BMKDIR/version.json); fi
  local OUTPUT=${APP}_summary.json
  echo -e "{ \"copies\" : $NCOPIES , \"threads_per_copy\" : $NTHREADS , \"events_per_thread\" : $NEVENTS_THREAD , \"throughput_score\" : $score , \"log\": \"$msg\", \"app\" : ${app} }" > ${OUTPUT} && cat ${OUTPUT}
  failedJson=${?}
  #-----------------------
  # Return status
  #-----------------------
  # Return 0 if result parsing and json generation were successful, 1 otherwise
  if [ $failedParse -eq 0 ] && [ $failedJson -eq 0 ]; then
    return 0
  else
    return 1
  fi
}
