HEPWL_BMKEXE=cms-gen-sim-bmk.sh
HEPWL_BMKOPTS="-t 4 -e 2" # -c replaces -n as of v0.12
HEPWL_BMKDIR=cms-gen-sim
HEPWL_BMKDESCRIPTION="CMS GEN-SIM of ttbar events, based on CMSSW_10_2_9"
HEPWL_DOCKERIMAGENAME=cms-gen-sim-bmk
HEPWL_DOCKERIMAGETAG=v1.0 # versions >= v0.13 use common bmk driver
HEPWL_CVMFSREPOS=cms.cern.ch
HEPWL_EXTEND_CMS_SPEC=./cms_spec_custom.txt
