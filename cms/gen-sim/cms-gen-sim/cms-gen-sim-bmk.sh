#!/bin/bash

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Extra CMS-GEN-SIM-specific setup
  export CMSSW_RELEASE=CMSSW_10_2_9
  export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
  source $VO_CMS_SW_DIR/cmsset_default.sh
  export SCRAM_ARCH=slc6_amd64_gcc700
  [[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE}
  pushd ${CMSSW_RELEASE}; eval `scramv1 runtime -sh`; popd
  # Configure WL copy
  ln -s ${BMKDIR}/data/GlobalTag.db ./GlobalTag.db
  CMSSW_CONF=TTbar_13TeV_TuneCUETP8M1_cfi_GEN_SIM.py
  JOB_EVENTS=$(( NEVENTS_THREAD * NTHREADS )) # bash shell arithmetic, may use var instead of $var
  cp ${BMKDIR}/${CMSSW_CONF}_template ./${CMSSW_CONF}
  sed -e "s@_NEVENTS_@${JOB_EVENTS}@g" -e "s@_NTHREADS_@$NTHREADS@g" -i ./${CMSSW_CONF}
  # Execute WL copy
  LOG=out_$1.log
  cmsRun ./${CMSSW_CONF} >>$LOG 2>&1 3>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Number of copies and number of threads per copy
  if [ "$USER_NTHREADS" != "" ] && [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$USER_NTHREADS
  elif [ "$USER_NTHREADS" != "" ]; then
    NTHREADS=$USER_NTHREADS
    NCOPIES=$((`nproc`/$NTHREADS))
  elif [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$((`nproc`/$NCOPIES))
  fi
  # Number of events per thread
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Return 0 if input arguments are valid, 1 otherwise
  # Report any issues to parseResults via s_msg
  export s_msg="ok"
  tot_load=$(($NCOPIES*$NTHREADS))
  if [ $tot_load -gt `nproc` ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load > number of available cores (`nproc`)"
    return 1
  elif [ $tot_load -eq 0 ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load. Please fix it"
    return 1
  elif [ $tot_load -ne `nproc` ];
  then s_msg="[WARNING] NCOPIES*NTHREADS ($NCOPIES*$NTHREADS=$tot_load) != `nproc` (number of available cores nproc)"
    echo $s_msg
  fi
  return 0
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=4
NCOPIES=$(( `nproc` / $NTHREADS ))
NEVENTS_THREAD=100
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > nproc
  NCOPIES=1
  NTHREADS=`nproc`
fi

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
