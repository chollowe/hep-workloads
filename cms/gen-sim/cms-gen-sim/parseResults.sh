function generateSummary(){
  echo -e "{\"copies\":$NCOPIES , \"threads_per_copy\":$NTHREADS , \"events_per_thread\" : $NEVENTS_THREAD , \"wl-scores\": $res_score, \"wl-stats\": {\"throughput_score\": $res_thr , \"CPU_score\": $res_cpu }, \"log\": \"${s_msg}\", \"app\": `cat $BMKDIR/version.json` }" > ${APP}_summary.json
  cat ${APP}_summary.json
}

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# [NB: if a separate function generateSummary exists, it must be internally called by parseResults]
# Input argument $1: status code <fail> from validateInputArguments and doOne steps:
# - <fail> < 0: validateInputArguments failed
# - <fail> > 0: doOne failed (<fail> processes failed out of $NCOPIES)
# - <fail> = 0: OK
# Return value: please return 0 if parsing was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# The environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[parseresults] ERROR! Invalid arguments '$@' to parseResults"; return 1; fi
  echo "[parseResults] parse results and generate summary (previous status: $1)"
  echo "[parseResults] current directory: $(pwd)"
  export res_cpu='""'
  export res_thr='""'
  export res_score='""'
  export s_msg="ok"
  if [ "$1" -ne 0 ]; then
    echo "Previous steps failed: skip parsing, go to generateSummary"
    generateSummary # this has no return code
    return 1
  else
    #-----------------------
    # Parse results
    #-----------------------
    echo "[parseResults] parsing results from" proc_*/out_*.log
    # Documentation of cmssw time report at https://github.com/cms-sw/cmssw/blob/09c3fce6626f70fd04223e7dacebf0b485f73f54/FWCore/Services/plugins/Timing.cc#L240
    # Parsing  Event Throughput: xxxx ev/s
    res_thr=`grep -H "Event Throughput" proc_*/out_*.log | sed -e "s@[^:]*: Event Throughput: \([ 0-9\.]*\) ev/s@\1@" | awk 'BEGIN{amin=1000000;amax=0;count=0;}  { val=$1; a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} END{n = asort(a); if (n % 2) {   median=a[(n + 1) / 2]; } else {median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;};
printf "{\"score\": %.4f, \"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f}", sum, sum/count, median, amin, amax
}'  nevt=$NEVENTS_THREAD nthread=$NTHREADS || (echo "{}"; return 1)`
    STATUS_1=$?

    #Duplicating above parsing, as quick and dirty. SHoudl be replaced by a python parser
    res_score=`grep -H "Event Throughput" proc_*/out_*.log | sed -e "s@[^:]*: Event Throughput: \([ 0-9\.]*\) ev/s@\1@" | awk 'BEGIN{amin=1000000;amax=0;count=0;}  { val=$1; a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} END{n = asort(a); if (n % 2) {   median=a[(n + 1) / 2]; } else {median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;};
printf "{\"gen-sim\" :%.4f} ", sum
}'  nevt=$NEVENTS_THREAD nthread=$NTHREADS || (echo "{}"; return 1)`

    # Parsing  CPU Summary: \n- Total loop:: xxxx seconds of all CPUs
    res_cpu=`grep -H -A2 "CPU Summary" proc_*/out_*.log | grep "Total loop" | sed -e "s@.*\sTotal loop: \([ 0-9\.]*\)@\1@" | awk 'BEGIN{amin=1000000;amax=0;count=0;}  { val=nevt*nthread/$1; a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} END{n = asort(a); if (n % 2) {median=a[(n + 1) / 2]; } else {median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;};
printf "{\"score\": %.4f, \"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f}", sum, sum/count, median, amin, amax
}' nevt=$NEVENTS_THREAD nthread=$NTHREADS || (echo "{}"; return 1)`
    STATUS_2=$?
    [[ "$STATUS_1" == "0" ]] && [[ "$STATUS_2" == "0" ]]
    STATUS=$?
    [[ "$STATUS" != "0" ]] && export s_msg="ERROR"
    echo "[parseResults] parsing completed (status=$STATUS)"
    #-----------------------
    # Generate summary
    #-----------------------
    echo "[parseResults] generate summary"
    generateSummary # this has no return code
    #-----------------------
    # Return status
    #-----------------------
    # Return 0 if result parsing and json generation were successful, 1 otherwise
    return $STATUS
  fi
}
