HEPWL_BMKEXE=cms-reco-bmk.sh
HEPWL_BMKOPTS="-t 4 -e 3" # -c replaces -n as of v0.7
HEPWL_BMKDIR=cms-reco
HEPWL_BMKDESCRIPTION="CMS RECO of ttbar events, based on CMSSW_10_2_9"
HEPWL_DOCKERIMAGENAME=cms-reco-bmk
HEPWL_DOCKERIMAGETAG=v1.0 # versions >= v0.8 use common bmk driver
HEPWL_CVMFSREPOS=cms.cern.ch
