HEPWL_BMKEXE=cms-digi-bmk.sh
HEPWL_BMKOPTS="-t 4 -e 20" # -c replaces -n as of v0.11 (NB the input file has only 100 events)
HEPWL_BMKDIR=cms-digi
HEPWL_BMKDESCRIPTION="CMS DIGI of ttbar events based on CMSSW_10_2_9"
HEPWL_DOCKERIMAGENAME=cms-digi-bmk
HEPWL_DOCKERIMAGETAG=v1.1 # versions >= v0.12 use common bmk driver
HEPWL_CVMFSREPOS=cms.cern.ch
