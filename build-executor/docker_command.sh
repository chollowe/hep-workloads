#!/bin/bash

scrdir=$(dirname $0)
cd $scrdir

gitrepo=$(git config --get remote.origin.url)
gitproj=${gitrepo#*gitlab.cern.ch*/}
if [ "${gitproj}" == "${gitrepo}" ]; then
  echo "ERROR! remote.origin.url '$gitrepo' does not seem to be at gitlab.cern.ch"
  exit 1
fi
gitproj=${gitproj%.git}
buildimg="gitlab-registry.cern.ch/${gitproj}/hep-workload-builder:latest"

if [ "$1" != "" ]; then 
  echo "To build the builder image locally (rather than in the registry via the CI), type:"
fi
echo "docker build -t $buildimg $scrdir/image"
if [ "$1" != "" ]; then 
  echo ""
  echo "To rebuild the builder image locally from scratch and ignore the docker cache, type:"
  echo "docker build --no-cache -t $buildimg $scrdir/image"
  echo ""
  echo "To pull the builder image locally from the remote registry, type:"
  echo "docker pull $scrdir/image"
fi

