#!/bin/bash

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Extra ATLAS-RECO-specific setup
  release=21.0.23
  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
  source $AtlasSetup/scripts/asetup.sh Athena,${release}
  # Configure WL copy
  DATADIR=/cvmfs/atlas.cern.ch/repo/benchmarks/hep-workloads/input-data/
  HighPtMinBiasHitsFile=$DATADIR/HITS.minbias_highpt.pool.root
  LowPtMinBiasHitsFile=$DATADIR/HITS.minbias_lowpt.pool.root
  HitsFile=$DATADIR/HITS.atlas.hepworkolad.pool.root
  unset FRONTIER_SERVER
  # Execute WL copy
  Reco_tf.py --inputHITSFile $HitsFile  --conditionsTag 'default:OFLCOND-MC16-SDR-14' --DBRelease '200.0.1'  --digiSeedOffset1 170 --digiSeedOffset2 170 \
    --inputHighPtMinbiasHitsFile $HighPtMinBiasHitsFile --inputLowPtMinbiasHitsFile $LowPtMinBiasHitsFile \
    --jobNumber 1 \
    --maxEvents $NEVENTS_THREAD --numberOfCavernBkg '0' --numberOfHighPtMinBias '0.122680569785' --numberOfLowPtMinBias '39.8773194302' --pileupFinalBunch '6' \
    --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];' 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools["MergeMcEventCollTool"].OnlySaveSignalTruth=True;job.StandardPileUpToolsAlg.PileUpTools["MdtDigitizationTool"].LastXing=150' 'RDOtoRDOTrigger:from AthenaCommon.AlgSequence import AlgSequence;AlgSequence().LVL1TGCTrigger.TILEMU=True;from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=False' 'ESDtoAOD:fixedAttrib=[s if "CONTAINER_SPLITLEVEL = '+"\'99\'"'" not in s else "" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' \
    --postInclude 'default:RecJobTransforms/UseFrontier.py' \
    --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True)' 'HITtoRDO:userRunLumiOverride={"run":222525, "startmu":40.0, "endmu":41.0, "stepmu":1.0, "startlb":1, "timestamp": 1376703331}' 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' 'ESDtoAOD:TriggerFlags.AODEDMSet="AODSLIM"' 'ESDtoAOD:TriggerFlags.AODEDMSet="AODSLIM"' \
    --geometryVersion 'default:ATLAS-R2-2016-01-00-01' --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInlcude.PileUpBunchTrainsMC16c_2017_Config1.py,RunDependentSimData/configLumi_muRange.py' \
    --ignoreErrors 'True' --mts ESD:0 --steering "RAWtoESD:in-RDO,in+RDO_TRIG,in-BS" \
    --outputRDOFile="myRDO.pool.root" --outputESDFile="myESD.pool.root" --outputAODFile="myAOD.pool.root" > out_$1.log 2>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=5

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
