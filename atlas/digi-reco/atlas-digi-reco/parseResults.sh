# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# [NB: if a separate function generateSummary exists, it must be internally called by parseResults]
# Input argument $1: status code <fail> from validateInputArguments and doOne steps:
# - <fail> < 0: validateInputArguments failed
# - <fail> > 0: doOne failed (<fail> processes failed out of $NCOPIES)
# - <fail> = 0: OK
# Return value: please return 0 if parsing was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# The environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[parseresults] ERROR! Invalid arguments '$@' to parseResults"; return 1; fi
  echo "[parseResults] parse results and generate summary (previous status: $1)"
  echo "[parseResults] current directory: $(pwd)"
  #-----------------------
  # Parse results
  #-----------------------
  STATUS=0
  parseOverallResults || STATUS=1
  parsePartialResults "HITtoRDO" || STATUS=1
  parsePartialResults "RDOtoRDOTrigger" || STATUS=1
  parsePartialResults "RAWtoESD" || STATUS=1
  parsePartialResults "ESDtoAOD" || STATUS=1
  #-----------------------
  # Generate summary
  #-----------------------
  echo "[parseResults] generate summary from" *-${APP}_summary.json
  res=`cat *-${APP}_summary.json | sed "s/^{//" | sed "s/}$/,/"` # Fix json lint (BMK-137)
  if [ ${PIPESTATUS[0]} -ne 0 ]; then
    res="\"[ERROR] Something went wrong in parsing the CPU score\""
    STATUS=1
  else
    res=$(echo $res) # remove newlines and collapse to a single line
    res=$(echo "{ $res" | sed "s/,$/}/")
  fi
  local OUTPUT=${APP}_summary.json
  echo -e "{\"copies\":$NCOPIES , \"threads_per_copy\":$NTHREADS , \"events_per_thread\" : $NEVENTS_THREAD , \"CPU_score\": $res , \"app\": \"DigitalReconstruction_21.0.23\"}" > $OUTPUT
  cat $OUTPUT
  #-----------------------
  # Return status
  #-----------------------
  # Return 0 if result parsing and json generation were successful, 1 otherwise
  return $STATUS
}


function parseOverallResults(){ # TODO to be calculated properly, as SCORE=score(seq(stage1,stage2,stage3,stage4))
  echo "[parseOverallResults] parsing overall results from" proc_*/log.*
  # Parsing  Event Throughput: xxxx ev/s
  res=`grep -A1 "INFO Statistics for 'evt'" proc_*/log.* | grep "<cpu>" | sed -e "s@[^(]*([[:blank:]]*\([ 0-9\.]*\) +/-.*@\1@" | awk '
    BEGIN{amin=1000000;amax=0;count=0;}
    { if ($1>0) {val=1./(int($1*10.)/10000.); a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} }
    END{sep=sprintf("%*s", 120, "");gsub(/ /, "*", sep);n=asort(a);
      if (n % 2) {
        median=a[(n + 1) / 2];
      } else {
        median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;
      }; printf "{\"score\": %.4f, \"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f}", sum, sum/count, median, amin, amax
    }' || ( echo "\"[ERROR] Something went wrong in parsing the CPU score\""; exit 1 )`
  STATUS=$?
  echo -e "{ \"All\": $res }" > All-${APP}_summary.json
  return $STATUS
}


function parsePartialResults(){
  echo "[parsePartialResults] parsing partial results from" proc_*/log.$1
  # Parsing  Event Throughput: xxxx ev/s
  res=`grep -A1 "INFO Statistics for 'evt'" proc_*/log.$1 | grep "<cpu>" | sed -e "s@[^(]*([[:blank:]]*\([ 0-9\.]*\) +/-.*@\1@" | awk '
    BEGIN{amin=1000000;amax=0;count=0;}
    { if ($1>0) {val=1./(int($1*10.)/10000.); a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} }
    END{sep=sprintf("%*s", 120, "");gsub(/ /, "*", sep);n=asort(a);
      if (n % 2) {
        median=a[(n + 1) / 2];
      } else {
        median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;
      }; printf "{\"score\": %.4f, \"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f}", sum, sum/count, median, amin, amax
    }' || ( echo "\"[ERROR] Something went wrong in parsing the CPU score\""; exit 1 )`
  STATUS=$?
  echo -e "{ \"$1\": $res }" > $1-${APP}_summary.json
  return $STATUS
}

