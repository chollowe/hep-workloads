HEPWL_BMKEXE=atlas-digi-reco-bmk.sh
HEPWL_BMKOPTS="-c 1 -e 2"
HEPWL_BMKDIR=atlas-digi-reco
HEPWL_BMKDESCRIPTION="ATLAS Digi Reco based on athena version 21.0.23"
HEPWL_DOCKERIMAGENAME=atlas-digi-reco-bmk
HEPWL_DOCKERIMAGETAG=v0.4 # versions >= v0.1 use common bmk driver, >= v0.2 renamed as atlas-digi-reco
HEPWL_CVMFSREPOS=atlas.cern.ch,atlas-condb.cern.ch
