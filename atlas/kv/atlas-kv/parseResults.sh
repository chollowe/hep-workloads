# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# [NB: if a separate function generateSummary exists, it must be internally called by parseResults]
# Input argument $1: status code <fail> from validateInputArguments and doOne steps:
# - <fail> < 0: validateInputArguments failed
# - <fail> > 0: doOne failed (<fail> processes failed out of $NCOPIES)
# - <fail> = 0: OK
# Return value: please return 0 if parsing was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# The environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[parseResults] ERROR! Invalid arguments '$@' to parseResults"; return 1; fi
  local failedProc=$1
  echo "[parseResults] parse results and generate summary (previous status: $1)"
  echo "[parseResults] current directory: $(pwd)"
  #-----------------------
  # Parse results
  #-----------------------
  echo "[parseResults] parsing results from" proc_*/out_*.log
  local STATUS=0
  # Parse 'Event Throughput: xxxx ev/s'
  res=`grep -A2 "INFO Statistics for 'evt'" proc_*/AtlasG4_trf.log | grep "<real>" | sed -e "s@[^(]*([[:blank:]]*\([ 0-9\.]*\) +/-.*@\1@" | awk 'BEGIN{amin=1000000;amax=0;count=0;}
{ val=1./(int($1)/1000.); a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val}
END{sep=sprintf("%*s", 120, "");gsub(/ /, "*", sep);
n = asort(a); if (n % 2) {
       median=a[(n + 1) / 2];
    } else {
        median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;};
printf "\"wl-scores\": {\"sim\": %.4f} , \"wl-stats\": {\"score\": %.4f, \"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f}", sum, sum, sum/count, median, amin, amax
}' || (STATUS=1; echo "\"wl-scores\": {}")`
  #-----------------------
  # Generate json summary
  #-----------------------
  # Generate the summary json
  echo -e "\n[parseResults] generate json summary"  
  local OUTPUT=${APP}_summary.json
  echo -e "{\"copies\":$NCOPIES , \"threads_per_copy\":1 , \"events_per_thread\" : $NEVENTS_THREAD , $res , \"app\":`cat $BMKDIR/version.json`}" > $OUTPUT
  cat $OUTPUT
  #-----------------------
  # Return status
  #-----------------------
  # Return 0 if result parsing and json generation were successful, 1 otherwise
  return $STATUS
}
