import os
import json
import sys
import time
import re
from collections import OrderedDict


def check(infile,scriptname,appname):
    """
    Checks if run was successful. If successful returns True.
    """
    try: 
      successfulrun = False
      datafile = open(infile, 'r')
      for line in datafile:
        if 'successful run' in line:
          successfulrun = True
          break
      if successfulrun:
        return True
      else:
        return False
    except IOError:
      print ("%s ERROR (%s-bmk): File %s not found!" %(scriptname,appname,infile))
      return False

def read_values(infile):
    """
    Read information about CPU, memory and number of processed events from logfile.
    """
    nbeventslines, cpulines, cpusyslines, vmemlines, rsslines, swaplines, evtmaxcpulines, walltimelines = ([] for i in range(8))
    nbevents, cpuAvg, cpuError, cpuSys, vmemPeak, vmemRSS, vmemSwap, evtMaxCpu, walltime = ([] for i in range(9))
    with open(infile,'r') as f:
       lines = f.readlines()
       for i, line in enumerate(lines):
          if 'INFO Statistics for \'evt\'' in line:
             nbeventslines.append(line)
             cpulines.append(lines[i+1].strip())
             cpusyslines.append(lines[i+3].strip())
          if 'INFO VmPeak' in line:   
             vmemlines.append(line)
          if 'INFO VmRSS' in line:
             rsslines.append(line)
          if 'INFO VmSwap'in line:
             swaplines.append(line)
          if 'step evt' in line:
             evtmaxcpulines.append(lines[i+2].strip())
          if 'snapshot_post_fin' in line:
             walltimelines.append(line)
    for line in nbeventslines:
       nbevents.append(re.findall(r'=\s(\d+)',str(line))[0])
    for line in cpulines:
       cpuvalues = re.findall(r'(\d+).(\d{3})',str(line))
       cpuAvg.append(float(cpuvalues[0][0]+"."+cpuvalues[0][1]))
       cpuError.append(float(cpuvalues[1][0]+"."+cpuvalues[1][1]))
    for line in cpusyslines:
       cpusysvalues = re.findall(r'(\d+).(\d{3})',str(line))
       cpuSys.append(float(cpusysvalues[0][0]+"."+cpusysvalues[0][1]))
    for line in vmemlines:
       vmemPeak.append(float(re.findall(r'(\d+) kB',str(line))[0]))
    for line in rsslines:
       vmemRSS.append(float(re.findall(r'(\d+) kB',str(line))[0]))
    for line in swaplines:
       vmemSwap.append(float(re.findall(r'(\d+) kB',str(line))[0]))
    for line in evtmaxcpulines:
       try: 
          maxvalues = re.findall(r'(\d+)(@)(\d+)',str(line))[0]
          evtmaxcpuvalue = str(maxvalues[0])+str(maxvalues[1])+str(maxvalues[2])
          evtMaxCpu.append(evtmaxcpuvalue)
       except: 
          continue
    for line in walltimelines:
       walltime.append(re.findall(r'(\d+)',str(line))[5])
    return nbevents, cpuAvg, cpuError, cpuSys, vmemPeak, vmemRSS, vmemSwap, evtMaxCpu, walltime


def generate_summary(statusvalue,unitvalue,values0,values1,values2,values3,values4,values5,values6,values7,values8,values9,values10):
    """
    Generate a dictionary with all the information.
    """
    summary = OrderedDict()
    summary["ncopies"] = int(os.environ['NCOPIES'])
    summary["threads_per_copy"] = int(os.environ['NTHREADS'])
    summary["events_per_thread"] = int(os.environ['NEVENTS_THREAD'])
    summary["status"] = {"successful/all":str(len([val for val in statusvalue if val == 1]))+"/"+str(len(statusvalue)), 
                         "status_copy":[val for val in statusvalue]}
    summary["events_proc_Athena(MP)"] = [int(val) for val in values0] 
    summary["CPU_score"] = {"unit":unitvalue, "score":float(values1[0]), "weighted_score":float(values1[1]), "avg":float(values1[2]), 
                            "median":float(values1[3]), "min":float(values1[4]), "max":float(values1[5]), "score_proc":[float(val) for val in values2]}
    summary["CPU_proc"] = {"cpuAvg":[val for val in values3], "cpuError":[val for val in values4], 
                           "cpuSys":[val for val in values5], "evtMaxCPU":[val for val in values6]}
    summary["Memory_proc"] = {"vmem":[val for val in values7], "RSS":[val for val in values8], "swap":[val for val in values9]}
    summary["walltime_proc"] = [float(val) for val in values10]
    summary["log"] = os.environ['s_msg']
    #summary["app"] = os.popen('cat $BMKDIR/version.json').read().strip()
    try: 
      with open(os.path.join(os.environ['BMKDIR'],"version")) as f:
        version_json = json.loads(f.read())
      summary["app"] = version_json
    except:
      summary["app"] = ''
    return summary


def save_output(data,appname,scriptname,outfile):
    """
    Save output to a JSON file.
    """
    jsonfile = json.dumps(data)
    fjson = open(outfile,"w")
    fjson.write(jsonfile)
    fjson.close()
    print ("%s INFO (%s-bmk): Summary placed in %s" %(scriptname,appname,outfile))


def main():
    """
    Main function where we parse results from a logfile and save them to JSON. 
    """

    # Exit code of this python script: 0=success, 1=failure (BMK-129)
    pythonstatus = 0
    
    # Environment variables
    basename = os.path.basename(__file__)
    basewdir = os.environ['BASE_WDIR']
    bmkdir   = os.environ['BMKDIR']
    app      = os.environ['APP']
    resJSON  = os.path.join(basewdir,app+"_summary_new.json")

    # Global variables
    logfile  = "log.EVNTtoHITS"
    scale    = 1000
    if scale == 1:
       unit = "evt/ms"
    elif scale == 1000:   
       unit = "evt/s"
    elif scale == 1000000:
       unit = "evt/ks"
    else:
      unit = ""
      print ("%s WARNING (%s-bmk): Scale %i does not have predefined unit! Please define it." %(basename,app,scale))

    # Find logfiles
    dirs = []
    for (dirpath, dirnames, filenames) in os.walk(basewdir):
        dirs.extend(dirnames)
        break
 
    # Parse results from logfiles
    #print ("Parsing results from %s" %([os.path.join(basewdir,d,logfile) for d in dirs]))
    runstatus, nevt, scores, scores_wAvg, cpus, cpusError, cpusSys, evtMaxCpus, vmems, RSSs, swaps, walltimes = ([] for i in range(12))
    for d in dirs:
       logfilepath = os.path.join(basewdir,d,logfile)
       print ("Parsing results from %s" %(logfilepath))
       if check(logfilepath,basename,app): 
          print ("%s INFO (%s-bmk): Run was successful." %(basename,app)) 
          runstatus.append(1)
       else: 
          print ("%s ERROR (%s-bmk): Run was not successful!" %(basename,app))
          runstatus.append(0)
	  pythonstatus = 1
          continue
       nevt_proc, cpu_proc, cpuError_proc, cpuSys_proc, vmem_proc, rss_proc, swap_proc, evtmaxcpu_proc, walltime_proc = read_values(logfilepath)
       cpu_wAvg = 0.
       nevt_wAvg = 0.
 
       if len(cpu_proc)==len(cpuError_proc)==len(cpuSys_proc)==len(vmem_proc)==len(rss_proc)==len(swap_proc)==len(walltime_proc)==len(nevt_proc):
         for i in xrange(len(cpu_proc)):
           if cpu_proc[i] > 0:
             cpus.append(cpu_proc[i])
             scores.append(scale/cpu_proc[i])
             cpusError.append(cpuError_proc[i])
             cpusSys.append(cpuSys_proc[i])
             vmems.append(vmem_proc[i])
             RSSs.append(rss_proc[i])
             swaps.append(swap_proc[i])
             walltimes.append(walltime_proc[i])
             nevt.append(nevt_proc[i])
             nevt_wAvg += int(nevt_proc[i])
             cpu_wAvg += cpu_proc[i]*int(nevt_proc[i])
         if cpu_wAvg > 0:
           scores_wAvg.append(scale*len(evtmaxcpu_proc)*nevt_wAvg/cpu_wAvg)
         for i in xrange(len(evtmaxcpu_proc)):
           evtMaxCpus.append(evtmaxcpu_proc[i])
       else:
         print ("%s WARNING (%s-bmk): Number of threads is not consistent between different variables in %s! All results from this copy will be excluded." %(basename,app,d))

    scores_sorted = sorted(scores)
    scores_formatted = [ '%.4f' % elem for elem in scores ]

    # Calculate scores
    finalscore = 0.
    finalscore_wAvg = 0.
    avg = 0.
    median = 0.
    minimum = 0.
    maximum = 0.
    n = len(scores)
    if n > 0:
       minimum = min(scores)
       maximum = max(scores)
       for i in xrange(n):
          finalscore += scores[i]
       for i in xrange(len(scores_wAvg)):
          finalscore_wAvg += scores_wAvg[i]
       avg = finalscore/n
       if (n % 2 != 0):
          median = scores_sorted[(n+1)/2-1]
       else: 
          median = (scores_sorted[(n/2)-1] + scores_sorted[(n/2)])/2.
    scalevalues = [ '%.4f' %(finalscore), '%.4f' %(finalscore_wAvg), '%.4f' %(avg), '%.4f' %(median), '%.4f' %(minimum), '%.4f' %(maximum) ]

    # Generate summary and save to JSON file
    jsonSummary = generate_summary(runstatus, unit, nevt, scalevalues, scores_formatted, cpus, 
                                   cpusError, cpusSys, evtMaxCpus, vmems, RSSs, swaps, walltimes)
    save_output(jsonSummary,app,basename,resJSON)
    print jsonSummary   

    # Check if JSON file was created 
    if not os.path.isfile(resJSON):
       print ("%s ERROR (%s-bmk): Something went wrong in parsing the CPU score. File path %s does not exist!" %(basename,app,resJSON))
       pythonstatus = 1

    # Exit code of this python script: 0=success, 1=failure (BMK-129)
    sys.exit(pythonstatus)
    
if '__main__' in __name__:
    main()
