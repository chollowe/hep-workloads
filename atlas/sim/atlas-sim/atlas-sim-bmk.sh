#!/bin/bash

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Extra ATLAS-SIM-specific setup
  release=21.0.15
  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
  asetup --cmtconfig=x86_64-slc6-gcc49-opt AtlasOffline,${release}
  unset FRONTIER_SERVER
  export ATHENA_PROC_NUMBER=$NTHREADS # env variable for ATHENA threads/process number
  # Configure WL copy
  inputdatadir=/cvmfs/atlas.cern.ch/repo/benchmarks/hep-workloads/input-data
  inputdata=$inputdatadir/EVNT.13043099._000859.pool.root.1
  # Execute WL copy
  Sim_tf.py --inputEVNTFile="$inputdata" --maxEvents="$(($NEVENTS_THREAD*$NTHREADS))" \
    --postInclude "default:RecJobTransforms/UseFrontier.py" \
    --preExec "EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)" "EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True" \
    --preInclude "EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py" \
    --skipEvents="0" --firstEvent="6160001" \
    --outputHITSFile='myHITS.pool.root' \
    --physicsList="FTFP_BERT_ATL_VALIDATION" \
    --randomSeed="6163" --DBRelease="100.0.2" --conditionsTag "default:OFLCOND-MC16-SDR-14" \
    --geometryVersion="default:ATLAS-R2-2016-01-00-01_VALIDATION" --runNumber="407343" --AMITag="s3126" --DataRunNumber="284500" \
    --simulator="FullG4" --truthStrategy="MC15aPlus" > out_$1.log 2>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Number of events per thread
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Number of copies and number of threads per copy
  if [ "$USER_NTHREADS" != "" ] && [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$USER_NTHREADS
  elif [ "$USER_NTHREADS" != "" ]; then
    NTHREADS=$USER_NTHREADS
    NCOPIES=$((`nproc`/$NTHREADS))
  elif [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$((`nproc`/$NCOPIES))
  fi
  # Return 0 if input arguments are valid, 1 otherwise
  # Report any issues to parseResults via s_msg
  export s_msg="ok"
  tot_load=$(($NCOPIES*$NTHREADS))
  if [ $tot_load -gt `nproc` ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load > number of available cores (`nproc`)"
    return 1
  elif [ $tot_load -eq 0 ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load. Please fix it"
    return 1
  elif [ $tot_load -ne `nproc` ];
  then s_msg="[WARNING] NCOPIES*NTHREADS ($NCOPIES*$NTHREADS=$tot_load) != `nproc` (number of available cores nproc)"
    echo $s_msg
  fi
  return 0
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=8
NCOPIES=$((`nproc`/$NTHREADS))
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > `nproc`
  NCOPIES=1
  NTHREADS=$((`nproc`/$NCOPIES))
fi
NEVENTS_THREAD=20

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
