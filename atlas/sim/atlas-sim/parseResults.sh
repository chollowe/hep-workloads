parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

function generateSummary(){
  echo -e "{\"copies\":$NCOPIES , \"threads_per_copy\":$NTHREADS , \"events_per_thread\" : $NEVENTS_THREAD , $resJSON , \"log\": \"${s_msg}\", \"app\":`cat $BMKDIR/version.json` }" > ${APP}_summary.json
  cat ${APP}_summary.json
}

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# [NB: if a separate function generateSummary exists, it must be internally called by parseResults]
# Input argument $1: status code <fail> from validateInputArguments and doOne steps:
# - <fail> < 0: validateInputArguments failed
# - <fail> > 0: doOne failed (<fail> processes failed out of $NCOPIES)
# - <fail> = 0: OK
# Return value: please return 0 if parsing was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# The environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[parseresults] ERROR! Invalid arguments '$@' to parseResults"; return 1; fi
  echo "[parseResults] parse results and generate summary (previous status: $1)"
  echo "[parseResults] current directory: $(pwd)"
  export resJSON='""'
  export s_msg="ok"
  if [ "$1" -ne 0 ]; then
    echo "Previous steps failed: skip parsing, go to generateSummary"
    s_msg="ERROR before parsing"
    generateSummary # this has no return code
    return 1
  else
    #-----------------------
    # Parse results (python)
    #-----------------------
    #echo "[parseResults] python parser starting"
    export BASE_WDIR=`pwd`
    #python ${parseResultsDir}/parseResults.py # same directory as parseResults.sh
    pystatus=$?
    #echo "[parseResults] python parser completed (status=$pystatus)"
    #-----------------------
    # Parse results (bash)
    #-----------------------
    echo "[parseResults] bash parser starting"
    echo "[parseResults] parsing results from" proc_*/log.EVNTtoHITS
    # Parsing Event Throughput: xxxx ev/s
    resJSON=`grep -A1 "INFO Statistics for 'evt'" proc_*/log.EVNTtoHITS | grep "<cpu>" | sed -e "s@[^(]*([[:blank:]]*\([ 0-9\.]*\) +/-.*@\1@" | awk '
      BEGIN{amin=1000000;amax=0;count=0;}
      { if ($1>0) {val=1./(int($1*10.)/10000.); a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} }
      END{sep=sprintf("%*s", 120, "");gsub(/ /, "*", sep);n=asort(a);
        if (n % 2) {
          median=a[(n + 1) / 2];
        } else {
          median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;
        }; 
printf "\"wl-scores\": {\"sim\": %.4f} , \"wl-stats\": {\"score\": %.4f, \"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f}", sum, sum, sum/count, median, amin, amax
       }' || (echo "\"wl-scores\":{}"; exit 1)`
    shstatus=$?
    [ "$shstatus" != "0" ] && s_msg="ERROR in parsing"
    echo "[parseResults] bash parser completed (status=$shstatus)"
    #-----------------------
    # Generate summary
    #-----------------------
    generateSummary # this has no return code
    #-----------------------
    # Return status
    #-----------------------
    # Return 0 if result parsing and json generation were successful, 1 otherwise
    if [ $pystatus -ne 0 ] || [ $shstatus -ne 0 ]; then
      echo "[parseResults] parsing completed (status=1)"
      return 1
    else
      echo "[parseResults] parsing completed (status=0)"
      return 0
    fi
  fi
}
