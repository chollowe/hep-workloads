HEPWL_BMKEXE=atlas-sim-bmk.sh 
HEPWL_BMKOPTS="-e 3 -t 2" # -c replaces -n as of v0.14
HEPWL_BMKDIR=atlas-sim
HEPWL_BMKDESCRIPTION="ATLAS Sim (GEANT4) based on athena version 21.0.15"
HEPWL_DOCKERIMAGENAME=atlas-sim-bmk
HEPWL_DOCKERIMAGETAG=v1.0 # versions >= v0.14 use common bmk driver
HEPWL_CVMFSREPOS=atlas.cern.ch,atlas-condb.cern.ch,sft.cern.ch
