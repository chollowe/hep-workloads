HEPWL_BMKEXE=atlas-gen-bmk.sh
HEPWL_BMKOPTS="-e 4" # -c replaces -n as of v0.11
HEPWL_BMKDIR=atlas-gen
HEPWL_BMKDESCRIPTION="ATLAS Event Generation based on athena version 19.2.5.5"
HEPWL_DOCKERIMAGENAME=atlas-gen-bmk
HEPWL_DOCKERIMAGETAG=v1.1 # versions >= v0.11 use common bmk driver
HEPWL_CVMFSREPOS=atlas.cern.ch,atlas-condb.cern.ch,sft.cern.ch
HEPWL_EXTEND_SFT_SPEC=./sft_spec_custom.txt
