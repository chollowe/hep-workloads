#!/bin/bash

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Extra ATLAS-GEN-specific setup
  release=19.2.5.5
  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
  source $AtlasSetup/scripts/asetup.sh AtlasProduction,${release}
  # Configure WL copy
  joboptions=$BMKDIR/auxfiles/MC15.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.py
  # Execute WL copy
  Generate_tf.py --ecmEnergy="13000"  --firstEvent="1" --jobConfig="$joboptions" \
    --maxEvents="$NEVENTS_THREAD" --outputEVNTFile="EVNT.10867034._003922.pool.root.1" --outputTXTFile="TXT.10867034._003922.tar.gz.1" \
    --randomSeed="9413" --runNumber="410501"  > out_$1.log 2>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=500

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
